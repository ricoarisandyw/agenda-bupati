package id.go.sidoarjokab.sikada.data

import id.go.sidoarjokab.sikada.data.model.History

class HistoryFilter {
    fun onlyNotOpen(mDataset:List<History>?):List<History>{
        if(mDataset!=null){
            return mDataset.filter { p->!p.opened}
        }
        return ArrayList<History>()
    }
}