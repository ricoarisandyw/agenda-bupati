package id.go.sidoarjokab.sikada.data.model

import id.go.sidoarjokab.sikada.helper.DateConverter
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * Created by wijaya on 11/13/2018.
 */
class Agenda{
    var id = 0
    var title: String? = null
    var description: String? = null
    var location: String? = null
    var instansi_pengundang: String? = null
    var file_undangan: String? = null
    var file_sambutan: String? = null
    var file_pendukung: String? = null
    var file_acara: String? = null
    var ajudan_id = 0
    var ajudan_wabup_id = 0
    var protokol:String? = null
    var kehadiran = 0
    var kehadiran_lain:String? = null
    var jenis_tamu = 0
    var jenis_event = 0
    var date_start: String? = null
    var date_end: String? = null
    var image: String? = null
    var status_id = 0

    fun getDateStartLocal():LocalDateTime{
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        //convert String to LocalDate
        val localDate = LocalDateTime.parse(date_start,formatter)
        return localDate
    }

    fun getDateEndLocal():LocalDateTime{
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        //convert String to LocalDate
        val localDate = LocalDateTime.parse(date_end,formatter)
        return localDate
    }

    fun startDate():String{
        return DateConverter(date_start).from("yyyy-MM-dd HH:mm:ss").toIndonesia()
    }

    fun endDate():String{
        return DateConverter(date_end).from("yyyy-MM-dd HH:mm:ss").toIndonesia()
    }
}