package id.go.sidoarjokab.sikada.data.model;

public class EventResponse {
    Agenda data;

    public EventResponse(Agenda data) {
        this.data = data;
    }

    public Agenda getData() {
        return data;
    }

    public void setData(Agenda data) {
        this.data = data;
    }
}
