package id.go.sidoarjokab.sikada.data.model;

public class Fixed {
    public static String JENIS_EVENT[] = {"","USULAN","AGENDA"};
    public static String STATUS_EVENT[] = {"BELUM DIPROSES","TIDAK MASUK","DIKIRIM","APPROVE","DITOLAK"};
    public static String JENIS_KEHADIRAN[] = {"BELUM DIPROSES","BUPATI","WAKIL BUPATI","APPROVE","DITOLAK"};
    public static String NAMA_BULAN[] = new String[]{
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "oktober",
            "November",
            "Desember"
    };

    public static class USER_LEVEL{
        public static int ADMIN = 0;
        public static int BUPATI = 1;
        public static int WABUP = 2;
        public static int AJUDAN_BUPATI = 11;
        public static int AJUDAN_WABUP = 22;
        public static int PROTOKOL = 4;
        public static int KABAG_PROTOKOL = 6;
        public static int KASUBBAG_PROTOKOL = 99;
        public static int NO_EDIT = 99;

    }

    public static class OFFLINE_DATA {
        public static String USER = "user";
        public static String AGENDA = "agenda";
        public static String SELECTED_AGENDA = "selected_agenda";
        public static String USERNAME  = "username";
        public static String PASSWORD = "password";
        public static String HISTORY_DB = "history_db";
    }

    public static class JENIS_TAMU{
        public static String VVIP = "VVIP";
        public static String VIP = "VIP";
        public static String BIASA = "BIASA";
    }

    public static class JENIS_EVENT{
        public static String AGENDA = "AGENDA";
        public static String USULAN = "USULAN";
    }
}
