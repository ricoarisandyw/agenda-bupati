package id.go.sidoarjokab.sikada.data.model;

import java.util.Map;

public class MasterData {
    Map<String,String> data;

    public MasterData(Map<String, String> data) {
        this.data = data;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
