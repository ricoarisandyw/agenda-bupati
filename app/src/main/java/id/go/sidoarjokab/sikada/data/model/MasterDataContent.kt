package id.go.sidoarjokab.sikada.data.model

class MasterDataContent {
    open var id = 0
    open var name = " "

    constructor(id: Int, name: String) {
        this.id = id
        this.name = name
    }
}