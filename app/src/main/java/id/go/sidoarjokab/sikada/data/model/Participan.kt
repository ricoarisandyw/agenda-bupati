package id.go.sidoarjokab.sikada.data.model

/**
 * Created by wijaya on 11/21/2018.
 */
class Participan {
    private var id = 0

    private var event_id = 0
    var email:String? = null
    var name:String? = null
    var company:String? = null
    var company_address:String? = null
    var position:String? = null
    var code:String? = null
}