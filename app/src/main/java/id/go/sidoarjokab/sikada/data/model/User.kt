package id.go.sidoarjokab.sikada.data.model

/**
 * Created by wijaya on 11/21/2018.
 */
class User{
    var id = 0
    var username:String = ""
    var password:String  = ""
    var name:String  = ""
    var level:String  = ""
    var company:String  = ""
    var company_address:String  = ""
    var position:String = ""
}