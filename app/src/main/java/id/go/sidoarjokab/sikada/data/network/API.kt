package id.go.sidoarjokab.sikada.data.network

import id.go.sidoarjokab.sikada.data.model.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by wijaya on 11/13/2018.
 */
interface API {
    //============EVENT
    @GET("events/list?format=json&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getAllEvents() : Call<List<Agenda>>

    @GET("events/list?format=json&&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getAllEventsByStartDate(@Query("start_date") start_date : String,@Query("auth_token") auth_token : String) : Call<List<Agenda>>

    @GET("events/single?format=json&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getEventsById(@Query("id") id : String,@Query("auth_token") auth_token : String) : Call<EventResponse>

    //============PARTICIPANS
    @GET("events/participant/{id_participan}?format=json&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getParticipansByIdEvent(@Path("id_participan") participan_id: Int,@Query("auth_token") auth_token : String) : Call<List<Participan>>

    @GET("events/list?format=json&limit=1000&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getAllEventByStartDateandEndDate(
            @Query("start_date") start_date : String,
            @Query("end_date") end_date : String,
            @Query("auth_token") auth_token : String
    ) : Call<List<Agenda>>
    //============USER
    @POST("users/login?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun login(
            @Field("username") username:String,
            @Field("password") password:String,
            @Field("deviceid") deviceid:String,
            @Query("auth_token") auth_token : String
    ) : Call<LoginResponse>

    @POST("users/registerfcm?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun registerFcm(
            @Field("username") username:String,
            @Field("id") id:String,
            @Field("fcm_client_id") fcm_client_id:String,
            @Query("auth_token") auth_token : String
    ) : Call<LoginResponse>
    //============EVENT PROCESS
    @POST("events/update-data?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun update(
            @Field("username") username:String,
            @Field("password") password:String,
            @Field("id") id_event:String,
            @Field("date_start") daet_start:String,
            @Field("date_end") date_end:String,
            @Field("protokol") protokol:String,
            @Field("kehadiran") kehadiran:String,
            @Field("kehadiran_lain") kehadiran_lain:String,
            @Field("ajudan_id") ajudan_id:String,
            @Field("ajudan_wabup_id") ajudan_wabup_id:String,
            @Field("jenis_event") jenis_event:String,
            @Query("auth_token") auth_token : String
    ) : Call<UpdateResponse>

    @POST("events/approve?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun approve(
            @Field("username") username:String,
            @Field("password") password:String,
            @Field("id") id_event:String,
            @Query("auth_token") auth_token : String
    ) : Call<Boolean>

    @POST("events/decline?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun decline(
            @Field("username") username:String,
            @Field("password") password:String,
            @Field("id") id_event:String,
            @Query("auth_token") auth_token : String
    ) : Call<Boolean>

    @POST("events/decline?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    @FormUrlEncoded
    fun change(
            @Field("username") username:String,
            @Field("password") password:String,
            @Field("id") id_event:String,
            @Field("date_start") date_start:String,
            @Field("date_end") date_end:String,
            @Query("auth_token") auth_token : String
    ) : Call<Boolean>

    //============MASTER DATA
    @GET("master-data/ajudan?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getAjudan() : Call<MasterData>

    @GET("master-data/ajudan-wabup?format=json&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getAjudanWabup() : Call<MasterData>

    @GET("master-data/protokol?format=json&authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getProtokol() : Call<MasterData>

    @GET("master-data/kehadiran?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getKehadiran() : Call<MasterData>

    @GET("master-data/jenis-event?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getJenisEvent() : Call<MasterData>

    @GET("master-data/jenis-tamu?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getJenisTamu() : Call<MasterData>

    @GET("master-data/user-level?authtoken=pozozaBlJXHnYxfd-VOEbxJ8ghMXnBhRW")
    fun getUserLevel() : Call<MasterData>
}