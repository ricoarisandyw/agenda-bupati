package id.go.sidoarjokab.sikada.data.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.URL;


/**
 * Created by wijaya on 11/26/2018.
 */

public class BitmapHelper {
    public static Bitmap fromURL(String url){
        try {
            URL address = new URL(url);
            Bitmap bmp = BitmapFactory.decodeStream(address.openConnection().getInputStream());
            return bmp;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
