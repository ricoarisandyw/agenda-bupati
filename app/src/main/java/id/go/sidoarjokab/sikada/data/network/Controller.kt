package id.go.sidoarjokab.sikada.data.network

import android.content.Context
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by wijaya on 11/13/2018.
 */
open class Controller{

    //indoartacitramedia.com/eventlog/web/api/events/list?format=json&time=2018-08-08&page=1
    private var retrofit: Retrofit? = null
    //private var BASE_URL = "http://www.indoartacitramedia.com/eventlog/web/api/"
    private var BASE_URL = "http://sikada.sidoarjokab.go.id/api/"
    //private var BASE_URL = "http://10.0.3.2/eventlog/web/api/"
    constructor(context: Context){
        var url = PreferenceHelper(context).load("BASE_URL")
        if(!url.equals("")){
            BASE_URL = url
        }
    }

    open fun getRetrofitInstance(): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit
    }
}