package id.go.sidoarjokab.sikada.data.network.firebase;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Map;

import id.go.sidoarjokab.sikada.R;
import id.go.sidoarjokab.sikada.data.HistoryFilter;
import id.go.sidoarjokab.sikada.data.model.Agenda;
import id.go.sidoarjokab.sikada.data.model.EventResponse;
import id.go.sidoarjokab.sikada.data.model.Fixed;
import id.go.sidoarjokab.sikada.data.model.History;
import id.go.sidoarjokab.sikada.data.network.API;
import id.go.sidoarjokab.sikada.data.network.Controller;
import id.go.sidoarjokab.sikada.helper.PreferenceHelper;
import id.go.sidoarjokab.sikada.view.detail.DetailEventActivity;
import id.go.sidoarjokab.sikada.view.history.HistoryActivity;
import id.go.sidoarjokab.sikada.view.main.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("___NEW TOKEN",s);
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fb", s).apply();
    }

    public String getFCM(Context context){
        return context.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String,String> data = remoteMessage.getData();
        Log.d("__Firebase Notif",remoteMessage.getData().toString());
        SharedPreferences sharedPref = getBaseContext().getSharedPreferences("mydata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        Agenda agenda = new Agenda();
        agenda.setId(Integer.parseInt(data.get("id")));
        String value = gson.toJson(agenda);
        editor.putString(Fixed.OFFLINE_DATA.SELECTED_AGENDA, value);
        editor.apply();

        History history = new History();
        history.id = Integer.parseInt(data.get("id"));
        history.message = data.get("message");
        history.url = data.get("url");
        history.time = data.get("time");
        history.type = data.get("type");
        history.opened = false;

        PreferenceHelper preferenceHelper = new PreferenceHelper(getBaseContext());
        List<History> mHistoryList = (List) preferenceHelper.loadObjList(Fixed.OFFLINE_DATA.HISTORY_DB,new TypeToken<List<History>>(){}.getType());
        List<History> newHistoryList = new HistoryFilter().onlyNotOpen(mHistoryList);
        newHistoryList.add(history);

        preferenceHelper.saveObj(Fixed.OFFLINE_DATA.HISTORY_DB,newHistoryList);

        Intent intent;
        if(newHistoryList.size()>1)
            intent = new Intent(this, HistoryActivity.class);
        else
            intent = new Intent(this, DetailEventActivity.class);


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);

            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(data.get("message"))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if(newHistoryList.size()>1){
            mBuilder.setContentText("Terdapat "+mHistoryList.size()+" pemberitahuan yang belum dibuka");
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.cancelAll();
        notificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }
}
