package id.go.sidoarjokab.sikada.helper;


import android.util.Log;

import id.go.sidoarjokab.sikada.data.model.Fixed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by wijaya on 11/26/2018.
 */

public class DateConverter {
    public static String YYYYMMDD = "yyyy-MM-dd";
    public static String YYYYMMDDHHMM = "yyyy-MM-dd hh:mm";
    public static String DDMMYYYY = "dd-MM-yyyy";
    public static String DDBulanYYYY = "dd-MM-yyyy";
    public String data;
    public String fromFormat;
    public Calendar calendar;

    public DateConverter(String data){
        this.data = data;
    }

    public DateConverter from(String pattern){
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        calendar = Calendar.getInstance();
        try {
            calendar.setTime(format.parse(data));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return this;
    }

    public String to(String pattern){
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(calendar);
    }

    public String toIndonesia(){
        return calendar.get(Calendar.DAY_OF_MONTH)+" "+Fixed.NAMA_BULAN[calendar.get(Calendar.MONTH)]+" "+calendar.get(Calendar.YEAR)+" "+String.format("%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }
}
