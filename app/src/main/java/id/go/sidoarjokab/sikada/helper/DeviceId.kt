package id.go.sidoarjokab.sikada.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.telephony.TelephonyManager


/**
 * Created by wijaya on 11/18/2018.
 */
class DeviceId {
    @SuppressLint("MissingPermission")
    open fun showDeviceId(activity: Activity) : String{
        var suid: String? = null
        val tManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        suid =  tManager.deviceId
        return suid
    }
}