package id.go.sidoarjokab.sikada.helper;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

public class HtmlConversion {
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }
    public static Intent fromUrl(String url){
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }
}
