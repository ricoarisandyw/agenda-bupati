package id.go.sidoarjokab.sikada.helper

import android.app.Activity
import com.google.gson.reflect.TypeToken

class MasterDataLoader(activity: Activity) {
    var MASTER_JENIS_KEGIATAN = (PreferenceHelper(activity).loadMap("master_jenis", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
    var MASTER_AJUDAN = (PreferenceHelper(activity).loadMap("master_ajudan", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
    var MASTER_AJUDAN_WABUP = (PreferenceHelper(activity).loadMap("master_ajudan_wabup", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
    var MASTER_PROTOKOL = (PreferenceHelper(activity).loadMap("master_protokol", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
    var MASTER_KEHADIRAN = (PreferenceHelper(activity).loadMap("master_kehadiran", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
    var MASTER_TAMU = (PreferenceHelper(activity).loadMap("master_tamu", object : TypeToken< Map<Int,String>>(){}.type) as Map<Int,String>)
}