package id.go.sidoarjokab.sikada.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

public class MoveAndFinish {
    public MoveAndFinish(Activity current, Class target){
        current.finish();
        current.startActivity(new Intent(current,target));
    }
}
