package id.go.sidoarjokab.sikada.helper

import id.go.sidoarjokab.sikada.data.model.MasterDataContent

class MyCollection {
    companion object {
        fun MapToList(input: Map<Int, String>): List<MasterDataContent> {
            var listData = ArrayList<MasterDataContent>()
            input.entries.map { e ->
                listData.add(MasterDataContent(e.key,e.value))
            }
            return listData
        }
    }
}