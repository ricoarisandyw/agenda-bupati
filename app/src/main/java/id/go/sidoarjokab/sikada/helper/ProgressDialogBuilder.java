package id.go.sidoarjokab.sikada.helper;

import android.app.Activity;
import android.support.v7.app.AlertDialog;

import id.go.sidoarjokab.sikada.R;

public class ProgressDialogBuilder {
    public static AlertDialog make(Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        //View view = getLayoutInflater().inflate(R.layout.progress);
        builder.setView(R.layout.progress);
        return builder.create();
    }
}