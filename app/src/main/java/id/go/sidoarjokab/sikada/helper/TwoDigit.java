package id.go.sidoarjokab.sikada.helper;

public class TwoDigit {
    public static String from(int value){
        return String.format("%02d", value);
    }
}
