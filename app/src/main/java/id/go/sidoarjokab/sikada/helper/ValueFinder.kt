package id.go.sidoarjokab.sikada.helper

import id.go.sidoarjokab.sikada.data.model.MasterDataContent

class ValueFinder {
    companion object {
        fun findById(id:Int,data:List<MasterDataContent>):String{
            return data.filter{ p -> p.id==id}.first().name
        }
    }
}