package id.go.sidoarjokab.sikada.view

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.iid.FirebaseInstanceId
import id.go.sidoarjokab.sikada.view.main.MainActivity
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Agenda
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.helper.MoveAndFinish
import id.go.sidoarjokab.sikada.helper.PreferenceHelper


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@SplashActivity, object: OnSuccessListener<InstanceIdResult>{
            override fun onSuccess(result: InstanceIdResult?) {
//                Log.d("___SPLASH TOKEN:",result?.token)
//                getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("fb", result?.token).apply()
//                Handler().postDelayed(object: Runnable {
//                    override fun run() {
//                        MoveAndFinish(this@SplashActivity,MainActivity::class.java)
//                    }
//                },1000)
            }
        }).addOnCompleteListener(OnCompleteListener { task->
            if(task.isSuccessful){
                Log.d("___SPLASH TOKEN:",task.getResult()?.token)
                getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("fb", task.getResult()?.token).apply()
                Handler().postDelayed(object: Runnable {
                    override fun run() {
                        MoveAndFinish(this@SplashActivity,MainActivity::class.java)
                    }
                },1000)
            }else {
                Log.d("___SPLASH TOKEN:","failed-get-token")
                getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("fb", "failed-get-token").apply()
                Handler().postDelayed(object: Runnable {
                    override fun run() {
                        MoveAndFinish(this@SplashActivity,MainActivity::class.java)
                    }
                },1000)
            }

        })
    }
}
