package id.go.sidoarjokab.sikada.view.detail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.CheckboxData
import kotlinx.android.synthetic.main.item_checkbox.view.*

class CheckboxAdapter(var myDataset: List<CheckboxData>) : RecyclerView.Adapter<CheckboxAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_checkbox, parent, false) as LinearLayout
        val vh = CheckboxAdapter.ViewHolder(v)
        return vh
    }

    //Init Data
    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        // each data item is just a string in this case
        val vCheckbox = v.item_checkbox
    }

    override fun getItemCount(): Int {
        if(myDataset==null)
            return 0
        else
            return myDataset.size
    }

    override fun onBindViewHolder(v: CheckboxAdapter.ViewHolder, position: Int) {
        if(myDataset[position].checked)
            v.vCheckbox.isChecked = true
        v.vCheckbox.setOnClickListener {
            if(myDataset[position].checked)
                myDataset[position].checked = false
            else
                myDataset[position].checked = true
        }
        v.vCheckbox.setText(myDataset[position].title)
    }

    fun getResult():String{
        var result = Gson().toJson(myDataset.filter { p->p.checked==true}.map { it.id.toString() })
        return result
    }
}