package id.go.sidoarjokab.sikada.view.detail

import id.go.sidoarjokab.sikada.data.model.FileEvent

/**
 * Created by wijaya on 11/26/2018.
 */
interface DetailContract {
    interface View{
        fun onItemClicked(file:FileEvent)
        fun onSaveEdit(startDate:String,endDate:String,kehadiran:String,ajudan:String)
    }
}