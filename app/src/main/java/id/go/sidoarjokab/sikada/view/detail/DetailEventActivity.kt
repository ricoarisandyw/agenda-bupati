package id.go.sidoarjokab.sikada.view.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Agenda
import id.go.sidoarjokab.sikada.data.model.EventResponse
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.data.model.User
import id.go.sidoarjokab.sikada.data.network.API
import id.go.sidoarjokab.sikada.data.network.APIHelper
import id.go.sidoarjokab.sikada.data.network.BitmapHelper
import id.go.sidoarjokab.sikada.data.network.Controller
import id.go.sidoarjokab.sikada.helper.HtmlConversion
import id.go.sidoarjokab.sikada.helper.MasterDataLoader
import id.go.sidoarjokab.sikada.helper.MoveAndFinish
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import id.go.sidoarjokab.sikada.view.edit.EditActivity
import id.go.sidoarjokab.sikada.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_detail_event.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.stream.Collector


class DetailEventActivity : AppCompatActivity(){

    var agenda: Agenda? = null
    lateinit var MASTER_DATA:MasterDataLoader
    var user:User?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_event)
        detail_toolbar.setNavigationIcon(resources.getDrawable(R.drawable.ic_arrow_back))
        setSupportActionBar(detail_toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        //SET MASTER DATA
        MASTER_DATA = MasterDataLoader(this)

        //EDIT PREVILLEGES
        user = PreferenceHelper(this).loadObj("user", User::class.java) as User?
        if(user!=null) {
            if(!(user?.level?.toInt()==Fixed.USER_LEVEL.ADMIN||
                    user?.level?.toInt()==Fixed.USER_LEVEL.AJUDAN_BUPATI||
                    user?.level?.toInt()==Fixed.USER_LEVEL.AJUDAN_WABUP||
                    user?.level?.toInt()==Fixed.USER_LEVEL.BUPATI||
                    user?.level?.toInt()==Fixed.USER_LEVEL.WABUP||
                    user?.level?.toInt()==Fixed.USER_LEVEL.PROTOKOL||
                    user?.level?.toInt()==Fixed.USER_LEVEL.KASUBBAG_PROTOKOL||
                    user?.level?.toInt()==Fixed.USER_LEVEL.KABAG_PROTOKOL
                            )){
                detail_file_undangan.visibility = View.GONE
                detail_file_pendukung.visibility = View.GONE
                detail_file_sambutan.visibility = View.GONE
                detail_edit.visibility = View.GONE
            }
        }else{
            detail_file_undangan.visibility = View.GONE
            detail_file_pendukung.visibility = View.GONE
            detail_file_sambutan.visibility = View.GONE
            detail_edit.visibility = View.GONE
        }
        detail_edit.setOnClickListener {
            MoveAndFinish(this,EditActivity::class.java)
        }

        agenda = PreferenceHelper(this).loadObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA, Agenda::class.java) as Agenda
        Controller(this).getRetrofitInstance()?.create(API::class.java)?.getEventsById(agenda?.id.toString(),"Token")?.enqueue(object : Callback<EventResponse>{
            override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                agenda = response.body()!!.data
                PreferenceHelper(this@DetailEventActivity).saveObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA,agenda)
                loadData()
            }

            override fun onFailure(call: Call<EventResponse>, t: Throwable) {
                loadData()
            }
        })
    }

    override fun onBackPressed() {
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right)
        MoveAndFinish(this@DetailEventActivity,MainActivity::class.java)
    }

    fun loadData(){
        //Setting url file
        detail_file_sambutan.setOnClickListener {
            var url = APIHelper().fileLoc(agenda!!.file_sambutan!!)
            Toast.makeText(this,url,Toast.LENGTH_LONG).show()
            openLink(url)
        }
        detail_file_pendukung.setOnClickListener {
            var url = APIHelper().fileLoc(agenda!!.file_pendukung!!)
            Toast.makeText(this,url,Toast.LENGTH_LONG).show()
            openLink(APIHelper().fileLoc(agenda!!.file_pendukung!!))
        }
        detail_file_undangan.setOnClickListener {
            var url = APIHelper().fileLoc(agenda!!.file_undangan!!)
            Toast.makeText(this,url,Toast.LENGTH_LONG).show()
            openLink(APIHelper().fileLoc(agenda!!.file_undangan!!))
        }

        detail_toolbar.title = agenda?.title
        detail_toolbar.subtitle = agenda?.startDate() + " - " + agenda?.endDate()
        detail_location.text = agenda?.location

        //Add detail information
        detail_description.text = ""
        var htmlDetailInfo:String = ""
        htmlDetailInfo+="<i><font color=\"#00964A\">Judul Lengkap</font></i><br>"+agenda!!.title+"<br><br>"
        htmlDetailInfo+="<i><font color=\"#00964A\">Tanggal Mulai</font></i><br>"+agenda!!.startDate()+"<br><br>"
        htmlDetailInfo+="<i><font color=\"#00964A\">Tanggal Akhir</font></i><br>"+agenda!!.endDate()+"<br><br>"
        htmlDetailInfo+="<i><font color=\"#00964A\">Deskripsi</font></i><br>"+agenda!!.description+"<br><br>"
//        htmlDetailInfo+="<i><font color=\"#00964A\">ID</font></i><br>"+agenda!!.id.toString()+"<br><br>"
//        htmlDetailInfo+="<i><font color=\"#00964A\">STATUS</font></i><br>"+ Fixed.STATUS_EVENT[agenda!!.status_id]+"<br><br>"
        if(user!=null) {
            htmlDetailInfo += "<i><font color=\"#00964A\">Jenis Kegiatan</font></i><br>" +
                    if (MASTER_DATA.MASTER_JENIS_KEGIATAN[agenda!!.jenis_event] != null) MASTER_DATA.MASTER_JENIS_KEGIATAN[agenda!!.jenis_event] + "<br><br>"
                    else "belum terisi" + "<br><br>"
            htmlDetailInfo += "<i><font color=\"#00964A\">Jenis Tamu</font></i><br>" +
                    if (MASTER_DATA.MASTER_TAMU[agenda!!.jenis_tamu] != null) MASTER_DATA.MASTER_TAMU[agenda!!.jenis_tamu] + "<br><br>"
                    else "belum terisi" + "<br><br>"
            htmlDetailInfo += "<i><font color=\"#00964A\">Instansi Pengundang</font></i><br>" + agenda!!.instansi_pengundang + "<br><br>"
            //LIST PROTOKOL
            var listProtokol = " "
            try{
                val listProtokolsData: List<Int>? = Gson().fromJson(agenda?.protokol, object : TypeToken<List<Int>>() {}.type) as List<Int>?

                if (listProtokolsData != null) {
                    if (listProtokolsData.size > 0)
                        for (protokolId in listProtokolsData!!) {
                            listProtokol += MASTER_DATA.MASTER_PROTOKOL[protokolId] + "<br>"
                        }
                }
            }catch (e:Exception){

            }

            htmlDetailInfo += "<i><font color=\"#00964A\">Protokol</font></i><br>" + listProtokol + "<br><br>"

            htmlDetailInfo += "<i><font color=\"#00964A\">Kehadiran Lain</font></i><br>" + agenda!!.kehadiran_lain + "<br><br>"
            if (agenda?.ajudan_id != null && MASTER_DATA.MASTER_AJUDAN[agenda!!.ajudan_id] != null)
                htmlDetailInfo += "<i><font color=\"#00964A\">Ajudan</font></i><br>" + MASTER_DATA.MASTER_AJUDAN[agenda!!.ajudan_id] + "<br><br>"
            else {
                htmlDetailInfo += "<i><font color=\"#00964A\">Ajudan</font></i><br> <br><br>"
            }
            if (agenda?.ajudan_wabup_id != null && MASTER_DATA.MASTER_AJUDAN_WABUP[agenda!!.ajudan_wabup_id] != null)
                htmlDetailInfo += "<i><font color=\"#00964A\">Ajudan</font></i><br>" + MASTER_DATA.MASTER_AJUDAN_WABUP[agenda!!.ajudan_wabup_id] + "<br><br>"
            else {
                htmlDetailInfo += "<i><font color=\"#00964A\">Ajudan</font></i><br> <br><br>"
            }
        }
        detail_description.text = HtmlConversion.fromHtml(htmlDetailInfo)

        //IMAGE
        try {
            detail_image.setImageBitmap(BitmapHelper.fromURL(APIHelper().fileLoc(agenda!!.file_acara!!)))
        }catch (e:Exception){

        }
        //KEHADIRAN
        if(agenda?.kehadiran==null||agenda!!.kehadiran==0){
            detail_kehadiran.setBackgroundResource(R.drawable.radius_red)
            detail_kehadiran.text = "Belum ada konfirmasi kehadiran"
        }else{
            detail_kehadiran.setBackgroundResource(R.drawable.radius_green)
            detail_kehadiran.text = MASTER_DATA.MASTER_KEHADIRAN[agenda!!.kehadiran]
        }
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.itemId === android.R.id.home) {
            MoveAndFinish(this,MainActivity::class.java)
        }
        return super.onOptionsItemSelected(menuItem)
    }

    fun openLink(url:String){
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
