package id.go.sidoarjokab.sikada.view.detail;

import java.util.List;

public interface EditDialogView {
    public void onDone(List<Integer> result);
}
