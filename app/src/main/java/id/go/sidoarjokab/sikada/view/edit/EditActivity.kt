package id.go.sidoarjokab.sikada.view.edit


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateFormat
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.*
import id.go.sidoarjokab.sikada.data.network.API
import id.go.sidoarjokab.sikada.data.network.Controller
import id.go.sidoarjokab.sikada.helper.*
import id.go.sidoarjokab.sikada.view.detail.CheckboxAdapter
import id.go.sidoarjokab.sikada.view.detail.DetailEventActivity
import kotlinx.android.synthetic.main.activity_edit_event.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class EditActivity : AppCompatActivity() {

    var calendar = Calendar.getInstance()
    lateinit var adapter_protokol: CheckboxAdapter
    var user: User? = null
    lateinit var MASTER_DATA:MasterDataLoader
    lateinit var agenda:Agenda
    lateinit var progress:AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_event)

        progress = ProgressDialogBuilder.make(this)

        user = PreferenceHelper(this).loadObj(Fixed.OFFLINE_DATA.USER,User::class.java) as User?

        //INIT MASTER DATA
        agenda = PreferenceHelper(this).loadObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA, Agenda::class.java) as Agenda
        MASTER_DATA = MasterDataLoader(this)

        edit_kehadiran_lain.setText(agenda.kehadiran_lain)
        Log.d("__Agenda",Gson().toJson(agenda))
        edit_tanggal_mulai.setText(DateConverter(agenda.date_start).from("yyyy-MM-dd hh:mm:ss").toIndonesia())
        edit_tanggal_akhir.setText(DateConverter(agenda.date_end).from("yyyy-MM-dd hh:mm:ss").toIndonesia())
        if(agenda.kehadiran!=null&&MASTER_DATA.MASTER_KEHADIRAN[agenda.kehadiran]!=null)
            edit_kehadiran.setSelection(
                    MyCollection.MapToList(MASTER_DATA.MASTER_KEHADIRAN).indexOf(
                            MasterDataContent(agenda.kehadiran,MASTER_DATA.MASTER_KEHADIRAN[agenda.kehadiran].toString())
                    ))
        if(agenda.ajudan_id!=null&&MASTER_DATA.MASTER_AJUDAN[agenda.ajudan_id]!=null)
            edit_ajudan.setSelection(MyCollection.MapToList(MASTER_DATA.MASTER_AJUDAN).indexOf(
                    MasterDataContent(agenda.kehadiran,MASTER_DATA.MASTER_AJUDAN[agenda.ajudan_id].toString())
            ))
        if(agenda.ajudan_wabup_id!=null&&MASTER_DATA.MASTER_AJUDAN_WABUP[agenda.ajudan_wabup_id]!=null)
            edit_ajudan_wabup.setSelection(MyCollection.MapToList(MASTER_DATA.MASTER_AJUDAN_WABUP).indexOf(
                    MasterDataContent(agenda.kehadiran,MASTER_DATA.MASTER_AJUDAN_WABUP[agenda.ajudan_wabup_id].toString())
            ))
        edit_jenis_kegiatan.setSelection(
                MyCollection.MapToList(MASTER_DATA.MASTER_JENIS_KEGIATAN).indexOf(
                        MasterDataContent(agenda.jenis_event,MASTER_DATA.MASTER_JENIS_KEGIATAN[agenda.jenis_event].toString())
                ))

        //INIT SELECTED DATA
        edit_kehadiran_lain.setText(agenda.kehadiran_lain)

        edit_batal.setOnClickListener { startActivity(Intent(this,DetailEventActivity::class.java))}

        //KEHADIRAN
        var MASTER_KEHADIRAN = MASTER_DATA.MASTER_KEHADIRAN.map { it.value }
        val adapterKehadiran = ArrayAdapter(this, android.R.layout.simple_spinner_item, MASTER_KEHADIRAN)
        adapterKehadiran.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        edit_kehadiran.adapter = adapterKehadiran
        edit_kehadiran.setSelection(MASTER_KEHADIRAN.indexOf(MASTER_DATA.MASTER_KEHADIRAN[agenda.kehadiran]))

        //AJUDAN
        var MASTER_AJUDAN = MyCollection.MapToList(MASTER_DATA.MASTER_AJUDAN).map { it.name }
        val adapterAjudan = ArrayAdapter(this, android.R.layout.simple_spinner_item, MASTER_AJUDAN )
        adapterAjudan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        edit_ajudan.adapter = adapterAjudan
        edit_ajudan.setSelection(MASTER_AJUDAN.indexOf(MASTER_DATA.MASTER_AJUDAN[agenda.ajudan_id]))

        //AJUDAN WABUP
        var MASTER_AJUDAN_WABUP = MyCollection.MapToList(MASTER_DATA.MASTER_AJUDAN_WABUP).map { it.name }
        val adapterAjudanWabup = ArrayAdapter(this, android.R.layout.simple_spinner_item, MASTER_AJUDAN_WABUP )
        adapterAjudanWabup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        edit_ajudan_wabup.adapter = adapterAjudanWabup
        edit_ajudan_wabup.setSelection(MASTER_AJUDAN_WABUP.indexOf(MASTER_DATA.MASTER_AJUDAN_WABUP[agenda.ajudan_wabup_id]))


        //JENIS KEGIATAN
        val MASTER_JENIS = MyCollection.MapToList(MASTER_DATA.MASTER_JENIS_KEGIATAN).map { it.name }
        val adapterJenis = ArrayAdapter(this, android.R.layout.simple_spinner_item, MASTER_JENIS)
        adapterJenis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        edit_jenis_kegiatan.adapter = adapterJenis
        edit_jenis_kegiatan.setSelection(MASTER_JENIS.indexOf(MASTER_DATA.MASTER_JENIS_KEGIATAN[agenda.jenis_event]))

        //DATA PROTOKOL
        edit_protokol.layoutManager = LinearLayoutManager(this)
        val protokolCheckBoxData = ArrayList<CheckboxData>()

        var listProtokolAgenda  = listOf("")
        try {
            listProtokolAgenda = Gson().fromJson(agenda.protokol, object : TypeToken<List<String>>() {}.type) as List<String>
        }catch(e:Exception){

        }

        for (dp in MyCollection.MapToList(MASTER_DATA.MASTER_PROTOKOL)){
            val cb = CheckboxData()
            cb.id = dp.id
            cb.title = dp.name
            cb.value = dp.name
            //jika id ada di dp list protokol
            if(listProtokolAgenda.contains(dp.id.toString())) cb.checked = true
            protokolCheckBoxData.add(cb)
        }
        adapter_protokol = CheckboxAdapter(protokolCheckBoxData)
        edit_protokol.adapter = adapter_protokol

        edit_tanggal_akhir.setOnClickListener {
            DatePickerDialog(this, DialogDateEnd, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        edit_tanggal_mulai.setOnClickListener {
            DatePickerDialog(this, DialogDateStart, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        //SAVE DATA
        edit_simpan.setOnClickListener {
            progress.show()
            update()
        }
    }

    fun findByValue(db:Map<Int,String>,value:String) : String{
        var filtered = db.filter { it.value.equals(value)}
        var keys_filtered = filtered.map { it.key }
        return keys_filtered[0].toString()
    }

    fun update(){
        Controller(this).getRetrofitInstance()!!.create(API::class.java).update(
                user!!.username,
                PreferenceHelper(this).load("password"),
                agenda.id.toString(),
                agenda.date_start.toString(),
                agenda.date_end.toString(),
                adapter_protokol.getResult(),
                findByValue(MASTER_DATA.MASTER_KEHADIRAN,edit_kehadiran.selectedItem.toString()),
                edit_kehadiran_lain.text.toString(),
                findByValue(MASTER_DATA.MASTER_AJUDAN,edit_ajudan.selectedItem.toString()),
                findByValue(MASTER_DATA.MASTER_AJUDAN_WABUP,edit_ajudan_wabup.selectedItem.toString()),
                findByValue(MASTER_DATA.MASTER_JENIS_KEGIATAN,edit_jenis_kegiatan.selectedItem.toString()),
                "Token"
        ).enqueue(object: Callback<UpdateResponse>{
            override fun onFailure(call: Call<UpdateResponse>, t: Throwable) {
                progress.dismiss()
                Toast.makeText(this@EditActivity,"Gagal update data, Coba kembali beberapa saat",Toast.LENGTH_LONG).show()
            }
            override fun onResponse(call: Call<UpdateResponse>, response: Response<UpdateResponse>) {
                progress.dismiss()
                if(response.isSuccessful){
                    Log.d("__RESPONSE","EDIT AGENDA"+ response.toString())
                    PreferenceHelper(this@EditActivity).saveObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA,response.body()?.event_data)
                    finish()
                    startActivity(Intent(this@EditActivity,DetailEventActivity::class.java))
                }else{
                    update()
                    Toast.makeText(this@EditActivity,"Gagal update data, Coba kembali beberapa saat",Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onBackPressed() {
        startActivity(Intent(this,DetailEventActivity::class.java))
        finish()
    }

    //TANGGAL AKHIR
    var DialogDateEnd: DatePickerDialog.OnDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
            TimePickerDialog(this@EditActivity, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                val result = year.toString() + "-" + TwoDigit.from(monthOfYear+1) + "-" + TwoDigit.from(dayOfMonth) + " " + TwoDigit.from(hourOfDay)+":"+TwoDigit.from(minute)
                agenda.date_end = result
                edit_tanggal_akhir.text =  DateConverter(result).from("yyyy-MM-dd hh:ss").toIndonesia()
            },calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this@EditActivity)).show()
        }
    }

    //TANGGAL MULAI
    var DialogDateStart: DatePickerDialog.OnDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            TimePickerDialog(this@EditActivity, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                val result = year.toString() + "-" + TwoDigit.from(monthOfYear+1) + "-" + TwoDigit.from(dayOfMonth) + " " + TwoDigit.from(hourOfDay)+":"+TwoDigit.from(minute)
                agenda.date_start = result
                edit_tanggal_mulai.text =  DateConverter(result).from("yyyy-MM-dd hh:ss").toIndonesia()
            },calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this@EditActivity)).show()
        }
    }
}
