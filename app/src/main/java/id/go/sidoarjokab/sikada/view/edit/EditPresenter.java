package id.go.sidoarjokab.sikada.view.edit;

import android.app.Activity;

import id.go.sidoarjokab.sikada.data.model.Agenda;
import id.go.sidoarjokab.sikada.data.network.API;
import id.go.sidoarjokab.sikada.data.network.Controller;
import id.go.sidoarjokab.sikada.helper.MyCollection;
import id.go.sidoarjokab.sikada.helper.PreferenceHelper;
import retrofit2.Call;
import retrofit2.Callback;

public class EditPresenter {
    public Activity activity;

    public EditPresenter(Activity activity) {
        this.activity = activity;
    }
}
