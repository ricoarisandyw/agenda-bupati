package id.go.sidoarjokab.sikada.view.history

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.reflect.TypeToken
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.data.model.History
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        var mHistoryList = PreferenceHelper(this).loadObjList(Fixed.OFFLINE_DATA.HISTORY_DB,object:TypeToken<List<History>>(){}.type) as List<History>

        history_list.layoutManager = LinearLayoutManager(this)
        history_list.adapter = HistoryAdapter(mHistoryList,this)
    }
}
