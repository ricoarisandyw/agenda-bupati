package id.go.sidoarjokab.sikada.view.history

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Agenda
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.data.model.History
import id.go.sidoarjokab.sikada.helper.MoveAndFinish
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import id.go.sidoarjokab.sikada.view.detail.DetailEventActivity
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(var mDataset:List<History>,var activity: Activity) :RecyclerView.Adapter<HistoryAdapter.ViewHolder>(){

    var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_history, parent, false) as LinearLayout
        val vh = ViewHolder(v)
        return vh
    }

    override fun getItemCount(): Int {
        return if (mDataset == null) 0 else mDataset!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var history = mDataset[position]
        holder.vTime.setText(history.time)
        holder.vMessage.setText(history.message)
        holder.vMessage.setOnClickListener {
            val agenda = Agenda()
            agenda.id = history.id
            val dataForDB = mDataset.toMutableList()
            dataForDB.remove(history)
            PreferenceHelper(activity.baseContext).saveObj(Fixed.OFFLINE_DATA.HISTORY_DB,dataForDB)
            PreferenceHelper(activity.baseContext).saveObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA,agenda)
            MoveAndFinish(activity,DetailEventActivity::class.java)
        }
    }

    class ViewHolder(v : View):RecyclerView.ViewHolder(v){
        val vMessage = v.item_history_message
        val vTime = v.item_history_date
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(activity, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}
