package id.go.sidoarjokab.sikada.view.login

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import id.go.sidoarjokab.sikada.view.main.MainActivity
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.LoginResponse
import id.go.sidoarjokab.sikada.data.model.User
import id.go.sidoarjokab.sikada.data.network.API
import id.go.sidoarjokab.sikada.data.network.Controller
import id.go.sidoarjokab.sikada.data.network.firebase.MyFirebaseMessagingService
import id.go.sidoarjokab.sikada.helper.DeviceId
import id.go.sidoarjokab.sikada.helper.MoveAndFinish
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import id.go.sidoarjokab.sikada.helper.ProgressDialogBuilder

import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(){

    lateinit var progressDialog: AlertDialog
    lateinit var BASE_URL:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressDialog = ProgressDialogBuilder.make(this)

        BASE_URL = PreferenceHelper(this).load("BASE_URL")

        init()
        login_button.setOnClickListener {
            overridePendingTransition(android.R.anim.fade_out,android.R.anim.fade_in)
            progressDialog.show()
            login()
        }
        login_cancel.setOnClickListener {
            overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right)
            MoveAndFinish(this,MainActivity::class.java)
        }
    }

    fun login(){
        Controller(this).getRetrofitInstance()!!.create(API::class.java).login(
                login_email.text.toString(),
                login_password.text.toString(),
                "1234",
                "Token"
        )!!.enqueue(object: Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                Toast.makeText(baseContext,"Failed to Login",Toast.LENGTH_LONG).show()
                progressDialog.dismiss()
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                if(response!!.isSuccessful){
                    if(response!!.body()!!.status) {
                        registerFcm(login_email.text.toString(),response.body()!!.user_data!!.id.toString(),MyFirebaseMessagingService().getFCM(this@LoginActivity))

                        PreferenceHelper(this@LoginActivity).saveObj("user", response.body()!!.user_data)
                        PreferenceHelper(this@LoginActivity).save("username", login_email.text.toString())
                        PreferenceHelper(this@LoginActivity).save("password", login_password.text.toString())

                        MoveAndFinish(this@LoginActivity,MainActivity::class.java)

                    }else{
                        Toast.makeText(this@LoginActivity,"Username atau Password salah",Toast.LENGTH_LONG).show()
                    }
                }
                progressDialog.dismiss()
            }
        })
    }

    fun registerFcm(username:String,id_user:String,fcm:String){
        MyFirebaseMessagingService().getFCM(this@LoginActivity)
        Controller(this).getRetrofitInstance()!!.create(API::class.java).registerFcm(username,id_user,fcm,"Token").enqueue(object: Callback<LoginResponse>{
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d("Reponse FCM","Failure")
                progressDialog.dismiss()
            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if(response.isSuccessful){
                    Log.d("Reponse FCM",response.message())
                    Toast.makeText(this@LoginActivity,"Sukses Login",Toast.LENGTH_LONG).show()
                    MoveAndFinish(this@LoginActivity, MainActivity::class.java)
                }else{
                    Log.d("Reponse FCM","Response FCM Failed")
                }
                progressDialog.dismiss()
            }
        })
    }

    fun init(){
        if(PreferenceHelper(this).loadObj("user", User::class.java)!=null){
            MoveAndFinish(this@LoginActivity, MainActivity::class.java)
        }
    }
}
