package id.go.sidoarjokab.sikada.view.main

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Agenda
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.helper.MasterDataLoader
import id.go.sidoarjokab.sikada.helper.PreferenceHelper
import id.go.sidoarjokab.sikada.view.detail.DetailEventActivity
import kotlinx.android.synthetic.main.item_event.view.*
import android.view.animation.AnimationUtils
import id.go.sidoarjokab.sikada.helper.MoveAndFinish


/**
 * Created by wijaya on 11/15/2018.
 */
class AgendaAdapter// Provide a suitable constructor (depends on the kind of dataset)
(myDataset: List<Agenda>, var activity: Activity) : RecyclerView.Adapter<AgendaAdapter.ViewHolder>() {
    var mDataset: List<id.go.sidoarjokab.sikada.data.model.Agenda> = myDataset
    var lastPosition = -1

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        // create a new view
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_event, parent, false) as LinearLayout
        val vh = ViewHolder(v)
        return vh
    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        // each data item is just a string in this case
        val vTitle = v.item_title
        val vTime = v.item_time
        val vKehadiran = v.item_participan
        val vBox = v.item_event_box
        val vLocation = v.item_lokasi
        val vLabel = v.item_event_color
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val agenda = mDataset[position]
        //CHANGE LABEL
        if(agenda.jenis_event==1)
            holder.vLabel.setBackgroundResource(R.drawable.radius_green)
        else
            holder.vLabel.setBackgroundResource(R.drawable.radius_blue)

        holder.vBox.setOnClickListener {
            PreferenceHelper(activity).saveObj(Fixed.OFFLINE_DATA.SELECTED_AGENDA,agenda)
            MoveAndFinish(activity, DetailEventActivity::class.java)
        }
        holder.vTitle.text = agenda.title
        holder.vTime.text = agenda.startDate()
        //PARTICIPANS
        if(agenda.kehadiran==null||agenda?.kehadiran==0){
            holder.vKehadiran.text = "Belum ada konfirmasi kehadiran"
        }else{
            holder.vKehadiran.text = MasterDataLoader(activity).MASTER_KEHADIRAN[agenda.kehadiran]
        }
        holder.vLocation.text = agenda.location
        setAnimation(holder.itemView, position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return if (mDataset == null) 0 else mDataset!!.size
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(activity, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}