package id.go.sidoarjokab.sikada.view.main

import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.Toast
import com.google.gson.reflect.TypeToken
import com.prolificinteractive.materialcalendarview.*
import com.prolificinteractive.materialcalendarview.spans.DotSpan
import id.go.sidoarjokab.sikada.R
import id.go.sidoarjokab.sikada.data.model.Agenda
import id.go.sidoarjokab.sikada.data.model.Fixed
import id.go.sidoarjokab.sikada.data.model.History
import id.go.sidoarjokab.sikada.data.model.User
import id.go.sidoarjokab.sikada.data.network.firebase.MyFirebaseMessagingService
import id.go.sidoarjokab.sikada.helper.*
import id.go.sidoarjokab.sikada.view.history.HistoryActivity
import id.go.sidoarjokab.sikada.view.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter


class MainActivity : AppCompatActivity(), MainContract.View {
    var dateNow = LocalDate.now()
    var format_yyyyMMdd = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    var now = dateNow.format(format_yyyyMMdd)
    var presenter: MainPresenter?=null
    var storedAgenda:List<Agenda>?=null
    lateinit var MASTER_DATA:MasterDataLoader
    var user:User?=null
    var first_init = true
    lateinit var progressDialog: AlertDialog
    lateinit var prefDB:PreferenceHelper
    lateinit var animIn:Animation
    lateinit var animOut:Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(this)
        prefDB = PreferenceHelper(this)

        animIn = AnimationUtils.loadAnimation(this,R.anim.abc_slide_in_top)
        animOut = AnimationUtils.loadAnimation(this,R.anim.abc_slide_out_top)

        Log.d("_____Main Token",MyFirebaseMessagingService().getFCM(this))

        progressDialog = ProgressDialogBuilder.make(this)

        main_toolbar.setNavigationIcon(resources.getDrawable(R.drawable.ic_date))
        setSupportActionBar(main_toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        main_agenda_list.layoutManager = LinearLayoutManager(this)

        main_calendar.setDateSelected(CalendarDay.today(),true)
        main_calendar_week.setDateSelected(CalendarDay.today(),true)
        Log.d("__CALENDAR","mode "+ main_calendar.selectionMode)
        Log.d("__CALENDAR WEEK","mode "+ main_calendar_week.selectionMode)

        main_close_calendar.setOnClickListener { openFullCalendar() }

        //CALENDAR CHANGE LISTENER
        main_calendar.setOnDateChangedListener { materialCalendarView, calendarDay, b ->
            Toast.makeText(baseContext, DateConverter(calendarDay.date.toString()).from("yyyy-MM-dd").toIndonesia(),Toast.LENGTH_LONG).show()
            main_calendar_week.setDateSelected(main_calendar_week.selectedDate,false)
            main_calendar_week.setDateSelected(calendarDay,true)
            main_calendar_week.setCurrentDate(calendarDay)
            openFullCalendar()
            progressDialog.show()
            presenter!!.loadSpecificData(calendarDay)
        }

        main_calendar_week.setOnDateChangedListener { materialCalendarView, calendarDay, b ->
            Toast.makeText(baseContext, DateConverter(calendarDay.date.toString()).from("yyyy-MM-dd").toIndonesia(),Toast.LENGTH_LONG).show()
            main_calendar.setDateSelected(main_calendar.selectedDate,false)
            main_calendar.setDateSelected(calendarDay,true)
            main_calendar.setCurrentDate(calendarDay)
            progressDialog.show()
            presenter!!.loadSpecificData(calendarDay)
        }

        progressDialog.show()
        presenter!!.loadBigData(now,dateNow.plusMonths(1).format(format_yyyyMMdd))
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when(menuItem.itemId) {
            android.R.id.home->openFullCalendar()
            R.id.main_menu_refresh->{
                progressDialog.show()
                presenter!!.loadBigData(now,dateNow.plusMonths(1).format(format_yyyyMMdd))
            }
            R.id.main_menu_agenda->{
                progressDialog.show()
                setAgendaList(storedAgenda!!.filter { p -> MASTER_DATA.MASTER_JENIS_KEGIATAN[p.jenis_event]== Fixed.JENIS_EVENT.AGENDA})
            }
            R.id.main_menu_usulan->{
                progressDialog.show()
                setAgendaList(storedAgenda!!.filter { p -> MASTER_DATA.MASTER_JENIS_KEGIATAN[p.jenis_event]== Fixed.JENIS_EVENT.USULAN})
            }
            R.id.main_menu_login->
                    if(user==null) {
                        MoveAndFinish(this, LoginActivity::class.java)
                    } else {
                        PreferenceHelper(this).save(Fixed.OFFLINE_DATA.USER,null)
                        MoveAndFinish(this, this::class.java)
                    }
            R.id.main_menu_history->
                MoveAndFinish(this, HistoryActivity::class.java)
            R.id.main_menu_server->{
                var input = EditTextDialog.build(baseContext)
                var alert = AlertDialog.Builder(this)
                alert.setTitle("Server URL")
                alert.setView(input)
                        .setPositiveButton("Set",object:DialogInterface.OnClickListener{
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                PreferenceHelper(this@MainActivity).save("BASE_URL",input.text.toString())
                                Toast.makeText(this@MainActivity,"Ganti server sukses",Toast.LENGTH_LONG)
                            }
                        })
                        .setNegativeButton("Cancel",object:DialogInterface.OnClickListener{
                            override fun onClick(dialog: DialogInterface?, which: Int) {

                            }
                        })
                alert.show()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        user = prefDB.loadObj(Fixed.OFFLINE_DATA.USER,User::class.java) as User?
        if(user!=null) {
            val userMenu = menu!!.findItem(R.id.main_menu_login)
            userMenu.setTitle("Logout (" + (user?.name)+")")
        }else{
            menu!!.findItem(R.id.main_menu_usulan).setVisible(false)
        }
        val historyMenu = menu!!.findItem(R.id.main_menu_history)
        val historyList = prefDB.loadObjList(Fixed.OFFLINE_DATA.HISTORY_DB,object:TypeToken<List<History>>(){}.type) as List<History>?
        if(historyList==null)
            historyMenu.setTitle("Riwayat (0)")
        else
            historyMenu.setTitle("Riwayat (" + historyList.size +")")

        return true
    }

    override fun openFullCalendar(){
        if(main_calendar.visibility==View.VISIBLE) {
            //SHOWING WEEK CALENDAR
            animOut.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationStart(animation: Animation?) {Log.d("__Calendar","Start SHOW WEEK CALENDAR")}
                override fun onAnimationEnd(animation: Animation?) {
                    main_calendar.visibility = View.GONE
                    main_close_calendar.visibility = View.GONE
                    Log.d("__Calendar","SHOW WEEK CALENDAR")

                    main_calendar_week.visibility = View.VISIBLE
                    main_calendar_week.startAnimation(animIn)
                }
            })
            //Begin Animation
            main_calendar.startAnimation(animOut)
        }else{
            //SHOWING MAIN CALENDAR
            animOut.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationStart(animation: Animation?) {Log.d("__Calendar","Start SHOW MAIN CALENDAR")}
                override fun onAnimationEnd(animation: Animation?) {
                    main_calendar_week.visibility = View.GONE
                    Log.d("__Calendar","SHOW MAIN CALENDAR")

                    main_calendar.visibility = View.VISIBLE
                    main_close_calendar.visibility = View.VISIBLE
                    main_calendar.startAnimation(animIn)
                }
            })
            //Begin Animation
            main_calendar_week.startAnimation(animOut)
        }
    }

    override fun onBackPressed() {
        if(main_calendar.visibility==View.VISIBLE){
            openFullCalendar()
        }else{
            super.onBackPressed()
        }
    }

    class HighlightDecorator : DayViewDecorator{
        lateinit var eventBg : Drawable
        var agendaList : List<Agenda>? = null

        override fun shouldDecorate(p0: CalendarDay): Boolean {
            if(agendaList?.filter { p->p.getDateStartLocal().toLocalDate()==p0.date}!!.size>0)
                return true
            return false
        }

        override fun decorate(p0: DayViewFacade?) {
            if (p0 != null){
                p0.addSpan(DotSpan(5f, R.color.sikada))
            }
        }
    }

    fun setAgendaList(agenda:List<Agenda>){
        var realData = agenda
        if(user!=null && agenda !=null){
            realData = agenda!!.filter { p ->  MASTER_DATA.MASTER_JENIS_KEGIATAN[p.jenis_event]== Fixed.JENIS_EVENT.AGENDA}
        }
        if(agenda.size == 0) {
            main_no_agenda.visibility = View.VISIBLE
            val adapter = AgendaAdapter(realData, this)
            main_agenda_list.adapter = adapter
        }else{
            main_no_agenda.visibility = View.GONE
            val adapter = AgendaAdapter(realData, this)
            main_agenda_list.adapter = adapter
        }
        progressDialog.dismiss()
    }

    override fun onBigDataLoaded(agenda: List<Agenda>) {
        progressDialog.dismiss()
        PreferenceHelper(this).saveObj(Fixed.OFFLINE_DATA.AGENDA,agenda)
        MASTER_DATA = MasterDataLoader(this)
        setAgendaList(agenda)
        storedAgenda = agenda


        //DECOR FULL CALENDAR
        val decor = HighlightDecorator()
        decor.eventBg = resources.getDrawable(R.drawable.border_radius_red)
        decor.agendaList = agenda

        main_calendar.addDecorator(decor)
        main_calendar_week.addDecorator(decor)

        if(first_init){
            first_init = false
            setAgendaList(agenda!!.filter { e->e.getDateStartLocal().dayOfMonth==dateNow.dayOfMonth&&dateNow.monthValue==e.getDateStartLocal().monthValue})
        }
    }

    override fun onSpesificDataLoaded(agenda: List<Agenda>, calendarDay:CalendarDay) {
        progressDialog.dismiss()
        PreferenceHelper(this).saveObj(Fixed.OFFLINE_DATA.AGENDA,agenda)
        storedAgenda = agenda

        //DECOR FULL CALENDAR
        val decor = HighlightDecorator()
        decor.eventBg = resources.getDrawable(R.drawable.border_radius_red)
        decor.agendaList = agenda

        main_calendar.addDecorator(decor)
        main_calendar_week.addDecorator(decor)

        setAgendaList(agenda!!.filter { e->e.getDateStartLocal().dayOfMonth==calendarDay.day&&e.getDateStartLocal().monthValue==calendarDay.month})
    }
}
