package id.go.sidoarjokab.sikada.view.main

import com.prolificinteractive.materialcalendarview.CalendarDay
import id.go.sidoarjokab.sikada.data.model.Agenda

/**
 * Created by wijaya on 11/26/2018.
 */
interface MainContract {
    interface View{
        fun onBigDataLoaded(agenda:List<Agenda>)
        fun onSpesificDataLoaded(agenda:List<Agenda>, calendarDay: CalendarDay)
        fun openFullCalendar()
    }
    interface Presenter{
        fun loadSpecificData(calendarDay: CalendarDay)
        fun loadBigData(date:String, date_end:String)
        fun offlineMode()
    }
}