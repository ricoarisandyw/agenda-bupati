package id.go.sidoarjokab.sikada.view.main;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import id.go.sidoarjokab.sikada.data.model.Agenda;
import id.go.sidoarjokab.sikada.data.model.MasterData;
import id.go.sidoarjokab.sikada.data.model.User;
import id.go.sidoarjokab.sikada.data.network.API;
import id.go.sidoarjokab.sikada.data.network.Controller;
import id.go.sidoarjokab.sikada.helper.MyToast;
import id.go.sidoarjokab.sikada.helper.PreferenceHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wijaya on 11/26/2018.
 */

public class MainPresenter implements MainContract.Presenter {
    MainContract.View view;
    public String BASE_URL;
    Context context;

    public MainPresenter(MainContract.View view) {
        this.view = view;
        this.context = ((Context)view);
        BASE_URL = new PreferenceHelper((Activity)view).load("BASE_URL");
    }


    @Override
    public void loadSpecificData(CalendarDay date) {
        //LOAD USER DATA
        new Controller(context).getRetrofitInstance().create(API.class).getAllEventByStartDateandEndDate(
                date.getDate().toString(),
                date.getDate().toString(),
                "alsidy8wd78a0d"
        ).enqueue(new Callback<List<Agenda>>() {
            @Override
            public void onResponse(Call<List<Agenda>> call, Response<List<Agenda>> response) {
                if(response.isSuccessful()){
                    new PreferenceHelper((Activity)view).saveObj("events",response.body());
                    Log.d("__SIKADA","Agenda Start Date: "+date_start);
                    Log.d("__SIKADA","Agenda End Date: "+date_end);
                    Log.d("__SIKADA","Agenda Specific Size : "+response.body().size());
                    view.onSpesificDataLoaded(response.body(),date);
                }else{
                    Log.e("Failure","Response Failure");
                    specificOffline(date);
                }
            }

            @Override
            public void onFailure(Call<List<Agenda>> call, Throwable t) {
                Log.e("Failure","Cannot get Events");
                MyToast.InternetFailure((Activity)view);
                specificOffline(date);
            }
        });
    }

    public void specificOffline(CalendarDay date){
        List<Agenda> events = (List) new PreferenceHelper((Activity) view).loadObjList("events", new TypeToken<ArrayList<Agenda>>(){}.getType());
        if(events!=null){
            view.onSpesificDataLoaded(events,date);
        }else{
            view.onSpesificDataLoaded(Collections.emptyList(),date);
        }
    }

    @Override
    public void offlineMode() {
        List<Agenda> events = (List) new PreferenceHelper((Activity) view).loadObjList("events", new TypeToken<ArrayList<Agenda>>(){}.getType());
        if(events!=null){
            view.onBigDataLoaded(events);
        }else{
            view.onBigDataLoaded(Collections.emptyList());
        }
    }

    String date_start,date_end;

    @Override
    public void loadBigData(String date_start,String date_end) {
        // === LOAD MASTER DATA ===
        this.date_start = date_start;
        this.date_end = date_end;
        loadMasterJenisKegiatan();
    }

    public void loadMasterJenisKegiatan(){
        //MASTER JENIS KEGIATAN
        new Controller(context).getRetrofitInstance().create(API.class).getJenisEvent().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_jenis",data);
                    loadMasterAjudan();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","JENIS KEGIATAN FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
    public void loadMasterAjudan(){
        //MASTER AJUDAN
        new Controller(context).getRetrofitInstance().create(API.class).getAjudan().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_ajudan",data);
                    loadMasterAjudanWabup();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","JENIS AJUDAN FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }

    public void loadMasterAjudanWabup(){
        //MASTER AJUDAN WABUP
        new Controller(context).getRetrofitInstance().create(API.class).getAjudanWabup().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Log.e("aaa", response.body().getData().toString());
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_ajudan_wabup",data);
                    loadMasterProtokol();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","JENIS AJUDAN WABUP FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
    public void loadMasterProtokol(){
        //MASTER PROTOKOL
        new Controller(context).getRetrofitInstance().create(API.class).getProtokol().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_protokol",data);
                    loadMasterKehadiran();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","MASTER PROTOKOL FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
    public void loadMasterKehadiran(){
        //MASTER KEHADIRAN
        new Controller(context).getRetrofitInstance().create(API.class).getKehadiran().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_kehadiran",data);
                    loadMasterTamu();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","MASTER KEHADIRAN FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
    public void loadMasterTamu(){
        //MASTER JENIS TAMU
        new Controller(context).getRetrofitInstance().create(API.class).getJenisTamu().enqueue(new Callback<MasterData>() {
            @Override
            public void onResponse(Call<MasterData> call, Response<MasterData> response) {
                if(response.isSuccessful()){
                    Map<String,String> data = response.body().getData();
                    new PreferenceHelper((Activity)view).saveObj("master_tamu",data);
                    loadAgenda();
                }else{
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<MasterData> call, Throwable t) {
                Log.e("__RESPONSE","MASTER TAMU FAIL");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
    public void loadAgenda(){
        // LOAD LIST AGENDA
        new Controller(context).getRetrofitInstance().create(API.class).getAllEventByStartDateandEndDate(
                date_start,
                date_end,
                "alsidy8wd78a0d"
        ).enqueue(new Callback<List<Agenda>>() {
            @Override
            public void onResponse(Call<List<Agenda>> call, Response<List<Agenda>> response) {
                Log.d("Response",response.toString());
                if(response.isSuccessful()){
                    new PreferenceHelper((Activity)view).saveObj("events",response.body());
                    Log.d("__SIKADA","Agenda Size : "+response.body().size());
                    view.onBigDataLoaded(response.body());
                }else{
                    Log.e("Failure","Response Failure");
                    offlineMode();
                }
            }

            @Override
            public void onFailure(Call<List<Agenda>> call, Throwable t) {
                Log.e("Failure","Cannot get Events");
                MyToast.InternetFailure((Activity)view);
                offlineMode();
            }
        });
    }
}
